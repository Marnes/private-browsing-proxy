const windows = require("sdk/windows").browserWindows;
const storage = require('sdk/simple-storage').storage;
const prefs = require('sdk/simple-prefs');
const config = require('sdk/preferences/service');
const notifications = require("sdk/notifications");
const privateBrowsing = require("sdk/private-browsing");
const {Cc, Ci} = require("chrome");

const debug = (...args) => {
	if (prefs.prefs.debug) console.debug(...args);
};

const manualConfigNames = ["http", "http_port", "share_proxy_settings", "ssl", "ssl_port", "ftp", "ftp_port", "socks", "socks_port", "socks_version", "no_proxies_on"];
const icons = {};

Object.prototype.forAll = (object, callback) => {
	for (let prop in object) {
		object.hasOwnProperty(prop) && callback(prop, object[prop]);
	}
};

const setConf = (name, value) => config.set("network.proxy." + name, value);
const getProxyType = () => config.get("network.proxy.type");
const setProxyType = type => config.set("network.proxy.type", type);
const setHomepage = url => config.set("browser.startup.homepage", url);
const isInPrivateBrowsing = () => Array.from(windows).findIndex(privateBrowsing.isPrivate) != -1;
const getIcon = isPrivate => icons[isPrivate? "on": "off"];
const getIcon64 = () => getIcon(isInPrivateBrowsing())["64"];

const setURLConfig = url => {
	config.set("network.proxy.autoconfig_url", url);
	notifications.notify({
		title: "Private Browsing Proxy",
		text: "Auto-URL setting changed to " + url,
		iconURL: getIcon64()
	});
};

const setManualConfig = conf => {
	conf.forAll(setConf);
	notifications.notify({
		title: "Private Browsing Proxy",
		text: "Manual proxy settings changed",
		iconURL: getIcon64()
	});
};

const humanReadableProxySetting = number => {
	switch(number) {
		case 0:
			return "No Proxy";
		case 1:
			return "Manual";
		case 2:
			return "Automatic URL";
		case 4:
			return "Auto-detect";
		case 5:
			return "System Proxy";
		default:
			throw "no such proxy setting";
	}
};

function proxyPrefListener(callback) {
	this.branch = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefService).getBranch("network.proxy.type");
	this.callback = callback;
	this.observe = (subject, topic, data) => topic == "nsPref:changed" && this.callback();
	this.branch.addObserver("", this, false);
	this.unobserve = () => this.branch.removeObserver("", this);
};

const listener = new proxyPrefListener(() => {
	notifications.notify({
		title: "Proxy settings changed",
		text: "Proxy changed to " + humanReadableProxySetting(config.get("network.proxy.type")),
		iconURL: getIcon64()
	});
});

const main = () => {
	const self = require("sdk/self");
	const panel = require("sdk/panel");
	const {ToggleButton} = require("sdk/ui/button/toggle");
	
	(() => {
		const sizes = ["18", "32", "36", "48", "64"];
		["off", "on"].forEach(state => {
			const icon = {};
			sizes.forEach(size => icon[size] = self.data.url("icon-" + state + "-" + size + ".png"));
			icons[state] = icon;
		});
	})();
	
	const getURLConfig = () => config.get("network.proxy.autoconfig_url");
	const getHomepage = () => config.get("browser.startup.homepage");
	const getManualConfig = () => {
		let conf = {};
		manualConfigNames.forEach(name => conf[name] = config.get("network.proxy." + name));
		return conf;
	};
	
	const assess = () => {
		const inPB = isInPrivateBrowsing();
		button.icon = getIcon(inPB);
		if (inPB != storage["lastDetectedPBState"]) {
			storage["lastDetectedPBState"] = inPB;
			if (prefs.prefs["privateHomepage"] != "") {
				if (inPB) {
					storage["userHomepage"] = getHomepage();
					setHomepage(prefs.prefs["privateHomepage"]);
				} else {
					setHomepage(storage["userHomepage"]);
				}
			}
			if (prefs.prefs["switchToAltProfileOnly"] && [1, 2].indexOf(getProxyType()) != -1) {
				if (inPB) {
					if (getProxyType() == 1) {
						storage["userManualConfig"] = getManualConfig();
						setManualConfig(storage["PBPManualConfig"]);
					} else {
						storage["userURLConfig"] = getURLConfig();
						setURLConfig(storage["PBPURLConfig"]);
					}
				} else {
					if (getProxyType() == 1) {
						setManualConfig(storage["userManualConfig"]);
					} else {
						setURLConfig(storage["userURLConfig"]);
					}
				}
			} else {
				if (inPB) {
					storage["userProxyConfig"] = getProxyType();
					setProxyType(prefs.prefs["desiredProxyConfig"]);
				} else {
					setProxyType(storage["userProxyConfig"]);
				}
			}
		}
	}
	
	const handleClick = state => state.checked && pnl.show({
		position: button
	});
	
	const handleHide = () => button.state("window", {
		checked: false
	});
	
	const pnl = panel.Panel({
		contentURL: self.data.url("panel.html"),
		contentScriptFile: self.data.url("panel.js"),
		onHide: handleHide
	});
	
	const button = ToggleButton({
		id: "private-browsing-proxy",
		label: "Configure PBP alt. profiles",
		icon: getIcon(false),
		onClick: handleClick
	});
	
	pnl.port.on("responseManualConfig", conf => {
		storage["PBPManualConfig"] = conf;
		pnl.port.emit("offerManualConfig", conf);
	});
	pnl.port.on("responseURLConfig", url => {
		storage["PBPURLConfig"] = url;
		pnl.port.emit("offerURLConfig", url);
	});
	pnl.port.on("responseSize", size => pnl.resize(size.width, size.height));
	pnl.on("show", () => pnl.port.emit("requestSize"));
	
	if (!storage["userManualConfig"]) storage["userManualConfig"] = getManualConfig();
	storage["PBPManualConfig"]? pnl.port.emit("offerManualConfig", storage["PBPManualConfig"]): pnl.port.emit("requestManualConfig");
	if (!storage["userURLConfig"]) storage["userURLConfig"] = getURLConfig();
	storage["PBPURLConfig"]? pnl.port.emit("offerURLConfig", storage["PBPURLConfig"]): pnl.port.emit("requestURLConfig");
	if (!storage["lastDetectedPBState"]) storage["lastDetectedPBState"] = isInPrivateBrowsing();
	if (!storage["userProxyConfig"]) storage["userProxyConfig"] = getProxyType();
	if (!storage["userHomepage"]) storage["userHomepage"] = getHomepage();
	
	assess();
	windows.on("open", assess);
	windows.on("close", assess);
};
exports.main = main;

const unload = () => {
	listener.unobserve();
	
	if (isInPrivateBrowsing()) {
		prefs.prefs["privateHomepage"] != "" && setHomepage(storage["userHomepage"]);
		getProxyType() == 1? setManualConfig(storage["userManualConfig"]): setURLConfig(storage["userURLConfig"]);
		setProxyType(storage["userProxyConfig"]);
	}
};
exports.onUnload = unload;