const manualConfigNames = ["http", "http_port", "share_proxy_settings", "ssl", "ssl_port", "ftp", "ftp_port", "socks", "socks_port", "socks_version", "no_proxies_on"];

function sendManualConfig() {
    self.port.emit("responseManualConfig", readManualConfig());
}

function readManualConfig() {
    let conf = {};
    for (let i = 0;i < manualConfigNames.length;i++) {
        let n = manualConfigNames[i];
        switch(n) {
            case "http_port":
            case "ssl_port":
            case "ftp_port":
            case "socks_port":
                conf[n] = parseInt(document.getElementById(n).value.replace(/^$/,"0"));
                break;
            case "share_proxy_settings":
                conf[n] = document.getElementById(n).checked;
                break;
            case "socks_version":
                conf[n] = document.getElementById("socksv4").checked? 4: 5;
                break;
            default:
                conf[n] = document.getElementById(n).value;
        }
    }
    return conf;
}

function writeManualConfig(config) {
    for (let c in config) {
        switch(c) {
            case "http_port":
            case "ssl_port":
            case "ftp_port":
            case "socks_port":
                document.getElementById(c).value = config[c].toString();
                break;
            case "share_proxy_settings":
                document.getElementById(c).checked = config[c];
                break;
            case "socks_version":
                document.getElementById("socksv" + config[c]).checked = true;
                break;
            default:
                document.getElementById(c).value = config[c];
        }
    }
}

function sendURLConfig() {
    self.port.emit("responseURLConfig", document.getElementById("url").value);
}

function writeURLConfig(url) {
    document.getElementById("url").value = url;
}

self.port.on("requestManualConfig", sendManualConfig);
self.port.on("offerManualConfig", writeManualConfig);
self.port.on("requestURLConfig", sendURLConfig);
self.port.on("offerURLConfig", writeURLConfig);

document.getElementById("config").addEventListener("submit", function(event) {
    sendManualConfig();
    sendURLConfig();
    event.preventDefault();
});

self.port.on("requestSize", function() {
    self.port.emit("responseSize", {
        width: document.documentElement.scrollWidth + 16,
        height: document.documentElement.scrollHeight + 16
    });
});